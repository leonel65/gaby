import 'package:blooming/user.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'user.dart';
void main(){
      WidgetsFlutterBinding.ensureInitialized();   SystemChrome.setPreferredOrientations([DeviceOrientation.landscapeRight]).then((_){
  runApp(MyApp());
  });
  
  }

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.teal
      ),
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home>  {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
     
      body: InkWell(
        onTap: (){
           Navigator.push(
            context,
             MaterialPageRoute(builder: (context) => User()),
          );
        },
        child: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage("assets/fond1.png"),
            fit: BoxFit.fill
        )
      ),
    ),
  )
     

    );

  }
}