import 'package:flutter/material.dart';


class User extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
    body:  InkWell(
        onTap: (){
           Navigator.push(
            context,
             MaterialPageRoute(builder: (context) => User()),
          );
        },
        child: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage("assets/07.png"),
            fit: BoxFit.fill
        )
      ),
    ),
  ),
    );
  }
}